package utils;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import org.testng.Assert;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;

public class CompareResults{
    public static void compareResultsFromAndroid() {
        try {
            String path = "src/main/resources/app/QRCode.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            result.getText().substring(0, 36);

            String path2 = "src/main/resources/app/QRCode2.jpg";
            BufferedImage bufferedImage2 = ImageIO.read(new FileInputStream(path2));
            BinaryBitmap binaryBitmap2 = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage2)));
            Result result2 = new MultiFormatReader().decode(binaryBitmap2);
            result2.getText().substring(0, 36);
            Assert.assertNotEquals(result.getText().substring(0, 36), result2.getText().substring(0, 36));
            System.out.println(result.getText().substring(0, 36));
            System.out.println(result2.getText().substring(0, 36));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void compareResultsFromIOS() {
        try {
            String path = "src/main/resources/ios/QRCode.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            result.getText().substring(0, 36);

            String path2 = "src/main/resources/ios/QRCode2.jpg";
            BufferedImage bufferedImage2 = ImageIO.read(new FileInputStream(path2));
            BinaryBitmap binaryBitmap2 = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage2)));
            Result result2 = new MultiFormatReader().decode(binaryBitmap2);
            result2.getText().substring(0, 36);
            Assert.assertNotEquals(result.getText().substring(0, 36), result2.getText().substring(0, 36));
            System.out.println(result.getText().substring(0, 36));
            System.out.println(result2.getText().substring(0, 36));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void compareResultsFromOfflineQRCode() {
        try {
            String path = "src/main/resources/ios/offlineQRCode.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            result.getText().substring(0, 36);

            String path2 = "src/main/resources/ios/offlineQRCode2.jpg";
            BufferedImage bufferedImage2 = ImageIO.read(new FileInputStream(path2));
            BinaryBitmap binaryBitmap2 = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage2)));
            Result result2 = new MultiFormatReader().decode(binaryBitmap2);
            result2.getText().substring(0, 36);
            Assert.assertEquals(result.getText().substring(0, 36), result2.getText().substring(0, 36));
            System.out.println(result.getText().substring(0, 36));
            System.out.println(result2.getText().substring(0, 36));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
