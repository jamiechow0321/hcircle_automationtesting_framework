package utils.android;
import driver.MobileType;
import driver.appiumDriverFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
public class AndroidBase{

    @BeforeClass
    public void ConfigureAppium(){
        driver.appDriver.openMobileApp(MobileType.Android);
    }

    @AfterClass
    public void tearDown() {
        appiumDriverFactory.androidDriver.quit();
    }
}
