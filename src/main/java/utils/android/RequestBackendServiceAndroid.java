package utils.android;

import config.ConfigReader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;
import static io.restassured.RestAssured.given;
public class RequestBackendServiceAndroid {
    private static final String devAuthBaseURI = "https://api.dev.exaleap.ai/auth";
    private static final String path = "src/main/resources/app/token.json";
    private static final String qrCodePath = "src/main/resources/app/qrcode.txt";
    private static final String qrCodePath2 = "src/main/resources/app/qrcode2.txt";
    private static final String qrCodeOfflinePath = "src/main/resources/app/qrcodeOffline.txt";
    private static final String devBaseURIACX = "https://api.dev.exaleap.ai/acx-simulator-svc";
    private static final String acxSimulator = "src/main/resources/app/acxSimulator.json";
    public static void POST_Authorize() {
        JSONObject request = new JSONObject();
        request.put("area_code", "852");
        request.put("phone_number", "33336868");
        request.put("organization_identifier", "hld");
        given().
                header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devAuthBaseURI + "/account/authorize").
                then().
                statusCode(200).log().all();
    }

    public static void POST_VerifyCode() {
        POST_Authorize();
        JSONObject request = new JSONObject();
        request.put("area_code", "852");
        request.put("phone_number", "33336868");
        request.put("verify_code", "111111");

        Response response = given().
                header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devAuthBaseURI + "/account/verify-code");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(path))) {
            printWriter.write(response.getBody().prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void POST_Simulator_qrCode_First() {
        POST_VerifyCode();

        File file = new File(qrCodePath);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String qrcodeString = scanner.nextLine();
        JSONParser jsonParser = new JSONParser();

        String accessToken = null;
        try {
            FileReader fileReader = new FileReader(path);
            Object object = jsonParser.parse(fileReader);
            JSONObject verifyCodeAPI = (JSONObject) object;
            JSONArray array = (JSONArray) verifyCodeAPI.get("result");
            for (int i = 0; i < array.size(); i++) {
                JSONObject result = (JSONObject) array.get(i);
                accessToken = (String) result.get("access_token");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String device_id = null;
        String device_data_interface = null;
        try {
            device_id = ConfigReader.getDeviceId();
            device_data_interface = ConfigReader.getDeviceDataInterface();
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject request = new JSONObject();
        request.put("device_id", device_id);
        request.put("device_data_interface", device_data_interface);
        request.put("code", qrcodeString);

        Response response = given().
                header("Authorization", "Bearer " + accessToken).contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devBaseURIACX + "/qrcode");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(acxSimulator))) {
            printWriter.write(response.getBody().prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void POST_Simulator_qrCode_Second() {
        POST_VerifyCode();

        File file = new File(qrCodePath2);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String qrcodeString = scanner.nextLine();
        JSONParser jsonParser = new JSONParser();

        String accessToken = null;
        try {
            FileReader fileReader = new FileReader(path);
            Object object = jsonParser.parse(fileReader);
            JSONObject verifyCodeAPI = (JSONObject) object;
            JSONArray array = (JSONArray) verifyCodeAPI.get("result");
            for (int i = 0; i < array.size(); i++) {
                JSONObject result = (JSONObject) array.get(i);
                accessToken = (String) result.get("access_token");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String device_id = null;
        String device_data_interface = null;
        try {
            device_id = ConfigReader.getDeviceId();
            device_data_interface = ConfigReader.getDeviceDataInterface();
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject request = new JSONObject();
        request.put("device_id", device_id);
        request.put("device_data_interface", device_data_interface);
        request.put("code", qrcodeString);

        Response response = given().
                header("Authorization", "Bearer " + accessToken).contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devBaseURIACX + "/qrcode");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(acxSimulator))) {
            printWriter.write(response.getBody().prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void POST_Simulator_qrCode_Offline() {
        POST_VerifyCode();

        File file = new File(qrCodeOfflinePath);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String qrcodeString = scanner.nextLine();
        JSONParser jsonParser = new JSONParser();

        String accessToken = null;
        try {
            FileReader fileReader = new FileReader(path);
            Object object = jsonParser.parse(fileReader);
            JSONObject verifyCodeAPI = (JSONObject) object;
            JSONArray array = (JSONArray) verifyCodeAPI.get("result");
            for (int i = 0; i < array.size(); i++) {
                JSONObject result = (JSONObject) array.get(i);
                accessToken = (String) result.get("access_token");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String device_id = null;
        String device_data_interface = null;
        try {
            device_id = ConfigReader.getDeviceId();
            device_data_interface = ConfigReader.getDeviceDataInterface();
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject request = new JSONObject();
        request.put("device_id", device_id);
        request.put("device_data_interface", device_data_interface);
        request.put("code", qrcodeString);

        Response response = given().
                header("Authorization", "Bearer " + accessToken).contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devBaseURIACX + "/qrcode");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(acxSimulator))) {
            printWriter.write(response.getBody().prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
