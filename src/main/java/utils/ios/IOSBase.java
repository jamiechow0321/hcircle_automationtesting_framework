package utils.ios;
import driver.MobileType;
import driver.appiumDriverFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class IOSBase extends appiumDriverFactory {
    @BeforeClass
    public void ConfigureAppium(){
        driver.appDriver.openMobileApp(MobileType.IOS);
    }

    @AfterClass
    public void tearDown()
    {
        appiumDriverFactory.iosDriver.quit();
    }
}
