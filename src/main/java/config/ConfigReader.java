package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

    public static Properties getProperties() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("src/main/java/config/data.properties");
        Properties properties = new Properties();
        properties.load(fileInputStream);
        return properties;
    }
    public static String getPlatformName() throws IOException {
        return getProperties().getProperty("platformName");
    }
    public static String getAutomationName() throws IOException {
        return getProperties().getProperty("automationName");
    }
    public static String getIOSDeviceName() throws IOException {
        return getProperties().getProperty("iOSDeviceName");
    }
    public static String getUdId() throws IOException {
        return getProperties().getProperty("udId");
    }
    public static String getBundleId() throws IOException {
        return getProperties().getProperty("bundleId");
    }
    public static String getRemoteURL() throws IOException {
        return getProperties().getProperty("remoteURL");
    }
    public static String getPhoneNum() throws IOException {
        return getProperties().getProperty("phoneNum");
    }
    public static String getPhoneOTP() throws IOException {
        return getProperties().getProperty("phoneOTP");
    }
    public static String getDeviceId() throws IOException {
        return getProperties().getProperty("deviceId");
    }
    public static String getDeviceDataInterface() throws IOException {
        return getProperties().getProperty("deviceDataInterface");
    }
    public static String getAndroidDeviceName() throws IOException {
        return getProperties().getProperty("androidDeviceName");
    }
    public static String getAndroidApk() throws IOException {
        return getProperties().getProperty("androidApk");
    }
    public static String getRegisterLastName() throws IOException {
        return getProperties().getProperty("registerLastName");
    }
    public static String getRegisterFirstName() throws IOException {
        return getProperties().getProperty("registerFirstName");
    }
    public static String getRegisterEmail() throws IOException {
        return getProperties().getProperty("registerEmail");
    }
    public static String getRegisterPhoneNum() throws IOException {
        return getProperties().getProperty("registerPhoneNum");
    }
}
