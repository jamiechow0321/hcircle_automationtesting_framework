package behavior;
import config.ConfigReader;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import driver.appiumDriverFactory;
import java.time.Duration;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

public class appBehavior {
    public static void stayBackground()
    {
        appiumDriverFactory.androidDriver.runAppInBackground(Duration.ofSeconds(5));
    }
    public static void openAirplaneMode()
    {
        appiumDriverFactory.androidDriver.openNotifications();
        appiumDriverFactory.androidDriver.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"飛行模式,關閉。,按鍵\"]")).click();
    }
    public static void closeAirplaneMode()
    {
        appiumDriverFactory.androidDriver.openNotifications();
        appiumDriverFactory.androidDriver.findElement(By.xpath("//android.view.ViewGroup[@content-desc=\"飛行模式,開啟。,按鍵\"]")).click();
    }

    public static void tapByCoordinates (int x,  int y) {
        new TouchAction(appiumDriverFactory.androidDriver)
                .tap(point(x,y))
                .waitAction(waitOptions(ofMillis(250))).perform();
    }

    public static void tapByCoordinatesIOS (int x,  int y) {
        new TouchAction(appiumDriverFactory.iosDriver)
                .tap(point(x,y))
                .waitAction(waitOptions(ofMillis(250))).perform();
    }

    public static void iphoneAlert()
    {
        appiumDriverFactory.iosDriver.switchTo().alert().accept();
    }

    public static void stayBackgroundIOS()
    {
        appiumDriverFactory.iosDriver.runAppInBackground(Duration.ofSeconds(5));
    }

    public static void scroll()
    {
        Dimension dimension = appiumDriverFactory.iosDriver.manage().window().getSize();
        System.out.println(dimension);
        int start_x = (int) (dimension.width * 1.0);
        int start_y = (int) (dimension.height * 0.0);

        int end_x = (int) (dimension.width * 0.5);
        int end_y = (int) (dimension.height * 0.5);

        TouchAction touchAction = new TouchAction(appiumDriverFactory.iosDriver);
        touchAction.press(PointOption.point(start_x, start_y)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(end_x, end_y)).release().perform();
    }
    public static void airplaneIOS()
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        appiumDriverFactory.iosDriver.findElement(By.xpath("//XCUIElementTypeSwitch[@name='airplane-mode-button']")).click();
    }

    public static void quitApp()
    {
        appiumDriverFactory.iosDriver.quit();
    }

    public static void removeApp()
    {
        try {
            appiumDriverFactory.iosDriver.removeApp(ConfigReader.getBundleId());
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
