package steps;

import pages.android.*;
import pages.ios.LoginPageIOS;
import pages.ios.qrCodePageIOS;

public class BaseStep {
    // Android
    public static LoginPageAndroid loginPageAndroid = new LoginPageAndroid();
    public static qrCodePageAndroid qrCodePageAndroid = new qrCodePageAndroid();
    public static LogoutPageAndroid logoutPageAndroid = new LogoutPageAndroid();
    public static HomePageAndroid homePageAndroid = new HomePageAndroid();
    public static ProfilePageAndroid profilePageAndroid = new ProfilePageAndroid();
    public static SettingsPageAndroid settingsPageAndroid = new SettingsPageAndroid();
    public static RegisterPageAndroid registerPageAndroid = new RegisterPageAndroid();
    public static HighlightsPageAndroid highlightsPageAndroid = new HighlightsPageAndroid();
//    public static FacilitiesPageAndroid facilitiesPageAndroid = new FacilitiesPageAndroid();
    // IOS
    public static LoginPageIOS loginPageIOS = new LoginPageIOS();
    public static qrCodePageIOS qrCodePageIOS = new qrCodePageIOS();
}
