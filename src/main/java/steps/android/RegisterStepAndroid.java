package steps.android;
import steps.BaseStep;
public class RegisterStepAndroid extends BaseStep{
    public static void LoginStepByRegister()
    {
        loginPageAndroid.clickLoginBtn();
        loginPageAndroid.inputPhoneNumberForRegister();
        loginPageAndroid.clickSendVerifyCodeBtn();
        loginPageAndroid.inputOTP();
    }
    public static void UserNewSignUp()
    {
        registerPageAndroid.inputName();
        registerPageAndroid.selectGender();
        registerPageAndroid.inputBirthday();
        registerPageAndroid.inputWorkEmail();
        registerPageAndroid.clickChkTnC();
        //registerPageAndroid.clickSubmitBtn();
    }
}
