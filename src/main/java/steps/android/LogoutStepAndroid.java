package steps.android;
import steps.BaseStep;
public class LogoutStepAndroid extends BaseStep{
    public static void logoutStep()
    {
        profilePageAndroid.clickSettings();
        settingsPageAndroid.clickLogout();
        logoutPageAndroid.clickLogoutConfirm();
        homePageAndroid.checkShowLoginBtnInHomepage();
    }
}
