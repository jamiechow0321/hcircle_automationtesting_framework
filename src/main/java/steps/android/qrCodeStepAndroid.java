package steps.android;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import steps.BaseStep;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

public class qrCodeStepAndroid extends BaseStep{
    private static final String qrCodePath = "src/main/resources/app/qrcode.txt";
    private static final String qrCodePath2 = "src/main/resources/app/qrcode2.txt";
    private static final String offlineQRCodePath = "src/main/resources/app/qrcodeOffline.txt";
    public static void qrCodeSteps()
    {
        // QR code
        profilePageAndroid.clickHomeButton();
        homePageAndroid.clickGreenQRCodeBallBtn();
        qrCodePageAndroid.checkProfilePhotoInCodePage();
        qrCodePageAndroid.checkUsernameInCodePage();
        qrCodePageAndroid.checkTenantInCodePage();
        qrCodePageAndroid.checkShowQRCodeToAccessHCircleServices();
        qrCodePageAndroid.capScreenshotQRCodeImg();
    }
    public static void readQRCode() {
        try {
            String path = "src/main/resources/app/QRCode.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(qrCodePath))){
                printWriter.write(result.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void readQRCode2() {

        qrCodePageAndroid.capScreenshotQRCode2Img();
        try {
            String path = "src/main/resources/app/QRCode2.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(qrCodePath2))){
                printWriter.write(result.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void verifyOfflineQRCode()
    {
        qrCodePageAndroid.isDisplayedOfflineIcon();
        qrCodePageAndroid.capScreenshotOfflineQRCodeImg();
    }
    public static void checkSuccessOpen()
    {
        qrCodePageAndroid.verifyResult();
    }
    public static void readOfflineQRCode()
    {
        try {
            String path = "src/main/resources/app/offlineQRCode.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(offlineQRCodePath))){
                printWriter.write(result.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
