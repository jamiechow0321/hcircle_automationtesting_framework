package steps.android;

import steps.BaseStep;

public class SettingsStepAndroid extends BaseStep{
    public static void changedLanguages()
    {
        // SC
        profilePageAndroid.clickSettings();
        settingsPageAndroid.clickLanguages();
        settingsPageAndroid.clickLanguagesSC();
        settingsPageAndroid.clickLanguagesCloseBtn();
        settingsPageAndroid.checkLanguagesSC();
        // EN
        settingsPageAndroid.clickLanguages();
        settingsPageAndroid.clickLanguagesEN();
        settingsPageAndroid.clickLanguagesCloseBtn();
        settingsPageAndroid.checkLanguagesEN();
        // TC
        settingsPageAndroid.clickLanguages();
        settingsPageAndroid.clickLanguagesTC();
        settingsPageAndroid.clickLanguagesCloseBtn();
        settingsPageAndroid.checkLanguagesTC();
    }
    public static void checkTnC()
    {
        settingsPageAndroid.clickTnCOnSettingsPage();
        settingsPageAndroid.clickPrivacyPolicy();
        settingsPageAndroid.clickAboutUs();
        settingsPageAndroid.clickContactUs();
        profilePageAndroid.clickBackInUpdateProfilePage();
    }
    public static void EnableAutoQRDisplayAtAppStart()
    {
        settingsPageAndroid.clickEnableAutoQRDisplayAtAppStart();
    }
    public static void checkUsernameInCodepage()
    {
        qrCodePageAndroid.checkUsernameInCodePage();
    }
}
