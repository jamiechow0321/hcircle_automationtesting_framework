package steps.android;

import steps.BaseStep;

public class ProfileStepAndroid extends BaseStep{

    public static void profileStep()
    {
        profilePageAndroid.checkTenant();
        profilePageAndroid.checkPhoneNum();
        profilePageAndroid.checkProfilePicture();
        profilePageAndroid.clickEditProfile();
        profilePageAndroid.changeFirstname();
        profilePageAndroid.clickSaveBtn();
        profilePageAndroid.clickBackInUpdateProfilePage();
        profilePageAndroid.checkChangedUsername();
        profilePageAndroid.clickEditProfile();
        profilePageAndroid.changeBackFirstname();
        profilePageAndroid.clickSaveBtn();
        profilePageAndroid.clickBackInUpdateProfilePage();
        profilePageAndroid.checkUsername();
    }
}
