package steps.android;

import steps.BaseStep;

public class HighlightsStepAndroid extends BaseStep{
    public static void highlightsStep()
    {
        profilePageAndroid.clickHomeButton();
        homePageAndroid.clickLatestNews();
        highlightsPageAndroid.highlightNews();
        highlightsPageAndroid.clickHomeBtn();
        homePageAndroid.clickLatestHappenings();
        highlightsPageAndroid.highlightHappenings();
        highlightsPageAndroid.clickHomeBtn();
        homePageAndroid.clickBanner();
        highlightsPageAndroid.highlightBanner();
        highlightsPageAndroid.clickCloseBtn();
    }
}
