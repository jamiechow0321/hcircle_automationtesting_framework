package steps.android;
import steps.BaseStep;

public class LoginStepAndroid extends BaseStep{
    public static void loginStep()
    {
        loginPageAndroid.clickLoginBtn();
        loginPageAndroid.inputPhoneNumber();
        loginPageAndroid.clickSendVerifyCodeBtn();
        loginPageAndroid.inputOTP();
        homePageAndroid.clickHomepageProfile();
        profilePageAndroid.checkUsername();
    }
}
