package steps.ios;
import steps.BaseStep;
public class LoginStepIOS extends BaseStep {

    public static void loginStep()
    {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.getStackTrace();
        }
        loginPageIOS.clickLoginBtn();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.getStackTrace();
        }

        loginPageIOS.inputPhoneNumber();
        loginPageIOS.clickSendVerifyCodeBtn();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        loginPageIOS.inputOTP();
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
