package steps.ios;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import steps.BaseStep;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

public class qrCodeStepIOS extends BaseStep {
    private static final String qrCodePath = "src/main/resources/ios/qrcode.txt";
    private static final String qrCodePath2 = "src/main/resources/ios/qrcode2.txt";
    private static final String offlineQRCodePath = "src/main/resources/ios/qrcodeOffline.txt";
    private static final String offlineQRCodePath2 = "src/main/resources/ios/qrcodeOffline2.txt";
    public static void clickGreenBallBtn()
    {
        qrCodePageIOS.clickGreenQRCodeBallBtn();
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void qrCodeSteps()
    {
        qrCodePageIOS.capScreenshotQRCodeImg();
    }
    public static void readQRCode() {
        try {
            String path = "src/main/resources/ios/QRCode.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(qrCodePath))){
                printWriter.write(result.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void readQRCode2() {
        qrCodePageIOS.capScreenshotQRCode2Img();
        try {
            String path = "src/main/resources/ios/QRCode2.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(qrCodePath2))){
                printWriter.write(result.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clickBtnClose()
    {
        qrCodePageIOS.clickCloseBtn();
    }

    public static void verifyOfflineQRCode()
    {
        qrCodePageIOS.isDisplayedOfflineIcon();
        qrCodePageIOS.capScreenshotOfflineQRCodeImgIOS();
    }

    public static void verifyOfflineQRCode2()
    {
        qrCodePageIOS.isDisplayedOfflineIcon();
        qrCodePageIOS.capScreenshotOfflineQRCodeImgIOS2();
    }
    public static void readOfflineQRCodeIOS()
    {
        try {
            String path = "src/main/resources/ios/offlineQRCode.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(offlineQRCodePath))){
                printWriter.write(result.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void readOfflineQRCodeIOS2()
    {
        try {
            String path = "src/main/resources/ios/offlineQRCode2.jpg";
            BufferedImage bufferedImage = ImageIO.read(new FileInputStream(path));
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(bufferedImage)));
            Result result = new MultiFormatReader().decode(binaryBitmap);
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(offlineQRCodePath2))){
                printWriter.write(result.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
