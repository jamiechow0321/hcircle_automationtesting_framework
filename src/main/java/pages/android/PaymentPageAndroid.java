package pages.android;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class PaymentPageAndroid extends BasePageAndroid{
    @AndroidFindBy(xpath = "")
    protected WebElement txtTnCInConfirmPurchase;
    @AndroidFindBy(xpath = "")
    protected WebElement txtCreditCard;
    @AndroidFindBy(xpath = "")
    protected WebElement btnPay;
    @AndroidFindBy(xpath = "")
    protected WebElement txtThankYouForPurchasing;
    @AndroidFindBy(xpath = "")
    protected WebElement btnGotIt;

    public void clickTnCInConfirmPurchasePage()
    {
        txtTnCInConfirmPurchase.click();
    }
    public void clickCreditCard()
    {
        txtCreditCard.click();
    }
    public void clickPay()
    {
        btnPay.click();
    }
    public void displayPaymentSuccessInfo()
    {
        txtThankYouForPurchasing.isDisplayed();
    }
    public void clickGotIt()
    {
        btnGotIt.click();
    }
}
