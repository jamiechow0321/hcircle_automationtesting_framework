package pages.android;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
public class LogoutPageAndroid extends BasePageAndroid{
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.Button[1]")
    protected WebElement btnLogoutAlertConfirm;
    public void clickLogoutConfirm()
    {
        btnLogoutAlertConfirm.click();
    }
}
