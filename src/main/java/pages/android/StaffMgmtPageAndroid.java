package pages.android;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class StaffMgmtPageAndroid extends BasePageAndroid{
    @AndroidFindBy(xpath = "")
    protected WebElement txtTenantManagement;
    @AndroidFindBy(xpath = "")
    protected WebElement txtTenant;
    @AndroidFindBy(xpath = "")
    protected WebElement btnHome;
    @AndroidFindBy(xpath = "")
    protected WebElement btnEdit;
    @AndroidFindBy(xpath = "")
    protected WebElement txtStaff;
    @AndroidFindBy(xpath = "")
    protected WebElement txtUserGroup;
    @AndroidFindBy(xpath = "")
    protected WebElement txtUserAccess;
    @AndroidFindBy(xpath = "")
    protected WebElement txtSearch;
    @AndroidFindBy(xpath = "")
    protected WebElement txtListSearchResult;
    @AndroidFindBy(xpath = "")
    protected WebElement btnDelete;
    @AndroidFindBy(xpath = "")
    protected WebElement btnRemove;
    @AndroidFindBy(xpath = "")
    protected WebElement txtRemoveSuccessToast;
    @AndroidFindBy(xpath = "")
    protected WebElement btnDone;
    //User Group
    @AndroidFindBy(xpath = "")
    protected WebElement btnAdmin;
    @AndroidFindBy(xpath = "")
    protected WebElement btnAddUser;
    @AndroidFindBy(xpath = "")
    protected WebElement btnSelect;
    @AndroidFindBy(xpath = "")
    protected WebElement btnAdd;
    //Access
    @AndroidFindBy(xpath = "")
    protected WebElement txtFemaleToilet;
    @AndroidFindBy(xpath = "")
    protected WebElement txtMaleToilet;
}
