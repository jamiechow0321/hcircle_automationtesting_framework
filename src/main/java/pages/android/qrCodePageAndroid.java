package pages.android;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.File;

public class qrCodePageAndroid extends BasePageAndroid {
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]")
    protected WebElement ProfilePhoto;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[1]")
    protected WebElement txtUsername;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[2]")
    protected WebElement txtTenant;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ImageView")
    protected WebElement imgOnlineQRCode;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView[3]")
    protected WebElement txtShowQRCodeToAccessHCircleServices;
//    @AndroidFindBy(xpath = "")
//    protected WebElement chkEnableAutoQRDisplayAtAppStart;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtOfflineModeKnowMore;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtAIAToilet;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtMFCToilet;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ImageView")
    protected WebElement imgOfflineQRCode;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView")
    protected WebElement imgChecked;

    public void checkProfilePhotoInCodePage()
    {
        ProfilePhoto.isDisplayed();
    }
    public void checkUsernameInCodePage()
    {
        Assert.assertEquals(txtUsername.getText(),"TEST DEV");
    }
    public void checkTenantInCodePage()
    {
        Assert.assertEquals(txtTenant.getText(),"Exaleap");
    }
    public void checkShowQRCodeToAccessHCircleServices()
    {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Assert.assertEquals(txtShowQRCodeToAccessHCircleServices.getText(),"出示二維碼使用\n" +
                "H·CIRCLE服務");
    }
//    public void displayEnableAutoQRDisplayAtAppStart()
//    {
//        chkEnableAutoQRDisplayAtAppStart.isDisplayed();
//    }
//    public void displayOfflineModeKnowMore()
//    {
//        txtOfflineModeKnowMore.isDisplayed();
//    }
//    public void displayOfflineModeAIAToilet()
//    {
//        txtAIAToilet.isDisplayed();
//    }
//    public void displayOfflineModeMFCToilet()
//    {
//        txtMFCToilet.isDisplayed();
//    }
    public void verifyResult()
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        imgChecked.isDisplayed();
    }
    public void capScreenshotQRCodeImg()
    {
        WebElement QRCode = imgOnlineQRCode;
        File elementScreenshot = QRCode.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(elementScreenshot, new File("src/main/resources/app/QRCode"+".jpg"));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
    public void capScreenshotQRCode2Img()
    {
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        WebElement QRCode2 = imgOnlineQRCode;
        File elementScreenshot2 = QRCode2.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(elementScreenshot2, new File("src/main/resources/app/QRCode2"+".jpg"));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
    public void isDisplayedOfflineIcon()
    {
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        imgOfflineQRCode.isDisplayed();
    }
    public void capScreenshotOfflineQRCodeImg()
    {
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        WebElement QRCode = imgOfflineQRCode;
        File elementScreenshot = QRCode.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(elementScreenshot, new File("src/main/resources/app/offlineQRCode"+".jpg"));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
