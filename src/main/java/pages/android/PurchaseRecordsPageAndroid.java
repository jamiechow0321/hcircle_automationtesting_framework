package pages.android;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class PurchaseRecordsPageAndroid extends BasePageAndroid{
    @AndroidFindBy(xpath = "")
    protected WebElement txtOrderId;
    @AndroidFindBy(xpath = "")
    protected WebElement txtPriceHKD;

    public void displayOrderId()
    {
        txtOrderId.isDisplayed();
    }
    public void displayPriceHKD()
    {
        txtPriceHKD.isDisplayed();
    }
}
