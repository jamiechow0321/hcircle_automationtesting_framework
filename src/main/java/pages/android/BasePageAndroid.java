package pages.android;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import driver.appiumDriverFactory;

public abstract class BasePageAndroid {
    public BasePageAndroid()
    {
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriverFactory.androidDriver), this);
    }
}
