//package pages.android;
//
//import io.appium.java_client.pagefactory.AndroidFindBy;
//import org.openqa.selenium.WebElement;
//
//public class FacilitiesPageAndroid extends BasePageAndroid{
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtAIATower;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtMFC;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtFitnessRoom;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtSwimmingPool;
//    @AndroidFindBy(xpath = "")
//    protected WebElement imgClubhouseBanner;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtOpen;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtFacilityInfo;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtValueTickets;
//
//    public void clickFitnessRoomOnAIATower()
//    {
//        txtFitnessRoom.click();
//    }
//    public void clickOpen()
//    {
//        txtOpen.click();
//    }
//    public void clickFacilityInfo()
//    {
//        txtFacilityInfo.click();
//    }
//    public void clickValueTickets()
//    {
//        txtValueTickets.click();
//    }
//    public void clickSwimmingPoolOnAIATower()
//    {
//        txtSwimmingPool.click();
//    }
//}
