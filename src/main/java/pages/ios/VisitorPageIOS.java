package pages.ios;//package pages.android;
//
//import io.appium.java_client.pagefactory.AndroidFindBy;
//import org.openqa.selenium.WebElement;
//
//public class VisitorPageAndroid extends BasePageAndroid{
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnAddVisitorInvite;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtSubject;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtBuilding;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnFloor;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnOneDayPass;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnMultiDayPass;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnDate;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnStartTime;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnEndTime;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnOK;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnAlertConfirmOK;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtAddVisitorInvitationSuccess;
//    @AndroidFindBy(xpath = "")
//    protected WebElement btnCopy;
//    @AndroidFindBy(xpath = "")
//    protected WebElement txtCopiedToast;
//
//    public void clickAddVisitorInvite()
//    {
//        btnAddVisitorInvite.click();
//    }
//    public void inputSubjectInAddVisitorInvitationPage()
//    {
//        txtSubject.sendKeys("TEST");
//    }
//    public void selectFloor()
//    {
//        btnFloor.click();
//    }
//    public void submitOK()
//    {
//        btnOK.click();
//    }
//    public void clickAlertConfirmOK()
//    {
//        btnAlertConfirmOK.click();
//    }
//    public void displayedAddVisitorInvitationSuccess()
//    {
//        txtAddVisitorInvitationSuccess.isDisplayed();
//    }
//    public void clickCopy()
//    {
//        btnCopy.click();
//    }
//    public void receivedToast()
//    {
//        txtCopiedToast.isDisplayed();
//    }
//}
