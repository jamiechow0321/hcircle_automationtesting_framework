package pages.ios;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import pages.android.BasePageAndroid;

public class ShuttleBusPageIOS extends BasePageAndroid {
    @AndroidFindBy(xpath = "")
    protected WebElement txtLastUpdateTime;
    @AndroidFindBy(xpath = "")
    protected WebElement txtBusToBus;
    @AndroidFindBy(xpath = "")
    protected WebElement txtBusToBusNotes;
    @AndroidFindBy(xpath = "")
    protected WebElement imgBusToBus;
    @AndroidFindBy(xpath = "")
    protected WebElement list4ShuttleBusInfo;
    //Route Info
    @AndroidFindBy(xpath = "")
    protected WebElement txtBusToBusInRouteInfo;
    @AndroidFindBy(xpath = "")
    protected WebElement txtBusToBusNotesInRouteInfo;
    @AndroidFindBy(xpath = "")
    protected WebElement txtBusToBusImgInRouteInfo;
    @AndroidFindBy(xpath = "")
    protected WebElement txtSchedule;
    @AndroidFindBy(xpath = "")
    protected WebElement txtMondayToFriday;
    @AndroidFindBy(xpath = "")
    protected WebElement txtHide;
    @AndroidFindBy(xpath = "")
    protected WebElement txtShow;
    @AndroidFindBy(xpath = "")
    protected WebElement txtShuttleBusTimeline;
    @AndroidFindBy(xpath = "")
    protected WebElement txtShuttleBusTnC;
    @AndroidFindBy(xpath = "")
    protected WebElement txtSaturday;
    @AndroidFindBy(xpath = "")
    protected WebElement txtBusStop;
    @AndroidFindBy(xpath = "")
    protected WebElement txtBusStopAddress;

    public void clickLastUpdateTime()
    {
        txtLastUpdateTime.click();
    }
    public void displayBusToBus()
    {
        txtBusToBus.isDisplayed();
    }
    public void displayBusToBusNotes()
    {
        txtBusToBusNotes.isDisplayed();
    }
    public void displayImgBusToBus()
    {
        imgBusToBus.isDisplayed();
    }
    public void displayBusToBusInRouteInfo()
    {
        txtBusToBusInRouteInfo.isDisplayed();
    }
    public void displayBusToBusNotesInRouteInfo()
    {
        txtBusToBusNotesInRouteInfo.isDisplayed();
    }
    public void displayBusToBusImgInRouteInfo()
    {
        txtBusToBusImgInRouteInfo.isDisplayed();
    }
    public void displayHide()
    {
        txtHide.isDisplayed();
    }
    public void clickMondayToFridayTab()
    {
        txtMondayToFriday.click();
    }
    public void displayShow()
    {
        txtShow.isDisplayed();
    }
    public void clickSaturdayTab()
    {
        txtSaturday.click();
    }
    public void displayShuttleBusTimeline()
    {
        txtShuttleBusTimeline.isDisplayed();
    }
    public void displayShuttleBusTnC()
    {
        txtShuttleBusTnC.isDisplayed();
    }
    public void clickBusStop()
    {
        txtBusStop.click();
    }
    public void displayBusStopAddress()
    {
        txtBusStopAddress.isDisplayed();
    }
}
