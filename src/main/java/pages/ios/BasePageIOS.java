package pages.ios;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import driver.appiumDriverFactory;

public abstract class BasePageIOS {

    public BasePageIOS()
    {
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriverFactory.iosDriver), this);
    }
}
