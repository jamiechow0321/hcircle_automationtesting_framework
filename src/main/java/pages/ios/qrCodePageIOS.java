package pages.ios;

import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import pages.ios.BasePageIOS;

import java.io.File;

public class qrCodePageIOS extends BasePageIOS {

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"DEV - H·Circle\"]/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]")
    protected WebElement btnGreenQRCodeBall;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"avatar TEST DEV Exaleap 出示二維碼使用\n" +
            "H·CIRCLE服務 打開程式後自動顯示二維碼\"]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeImage")
    protected WebElement imgQRCode;
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeImage")
    protected WebElement offlineImgQRCode;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='icon']")
    protected WebElement btnClose;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='了解更多']")
    protected WebElement iconOfflineLogo;

    public void clickCloseBtn()
    {
        btnClose.click();
    }

    public void clickGreenQRCodeBallBtn()
    {
        btnGreenQRCodeBall.click();
    }

    public void capScreenshotQRCodeImg()
    {
        WebElement QRCode = imgQRCode;
        File elementScreenshot = QRCode.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(elementScreenshot, new File("src/main/resources/ios/QRCode"+".jpg"));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void capScreenshotQRCode2Img()
    {
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        WebElement QRCode2 = imgQRCode;
        File elementScreenshot2 = QRCode2.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(elementScreenshot2, new File("src/main/resources/ios/QRCode2"+".jpg"));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void capScreenshotOfflineQRCodeImgIOS()
    {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        WebElement QRCode = offlineImgQRCode;
        File elementScreenshot = QRCode.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(elementScreenshot, new File("src/main/resources/ios/offlineQRCode"+".jpg"));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void capScreenshotOfflineQRCodeImgIOS2()
    {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        WebElement QRCode = offlineImgQRCode;
        File elementScreenshot = QRCode.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(elementScreenshot, new File("src/main/resources/ios/offlineQRCode2"+".jpg"));
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void isDisplayedOfflineIcon()
    {
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        iconOfflineLogo.isDisplayed();
    }
}
