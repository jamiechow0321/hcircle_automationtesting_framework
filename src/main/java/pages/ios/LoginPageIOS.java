package pages.ios;
import config.ConfigReader;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;

public class LoginPageIOS extends BasePageIOS {
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='登入']")
    protected WebElement btnLogin;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='icon icon'])[3]/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
    protected WebElement txtPhoneNum;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='送出手機驗證碼']")
    protected WebElement btnSendVerifyCode;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"|\"])[2]/XCUIElementTypeOther/XCUIElementTypeTextField")
    protected WebElement txtOTP;

    public void clickLoginBtn()
    {
        btnLogin.click();
    }
    public void inputPhoneNumber()
    {
        try {
            txtPhoneNum.sendKeys(ConfigReader.getPhoneNum());
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
    public void clickSendVerifyCodeBtn()
    {
        btnSendVerifyCode.click();
    }
    public void inputOTP()
    {
        try {
            txtOTP.sendKeys(ConfigReader.getPhoneOTP());
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
