package driver;

public enum MobileType {
    Android,
    IOS;
    private MobileType(){}
    public static MobileType getMobileType(String type)
    {
        if(type.equals("android"))
        {
            return Android;
        }else if(type.equals("ios"))
        {
            return IOS;
        }
        return null;
    }
}
