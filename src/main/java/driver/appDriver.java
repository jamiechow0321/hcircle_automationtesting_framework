package driver;

import config.ConfigReader;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.options.XCUITestOptions;
import java.net.URL;
import java.time.Duration;

public class appDriver {
    public static void openMobileApp(MobileType mobileType){

        if(mobileType.equals(MobileType.Android))
        {
            UiAutomator2Options options = new UiAutomator2Options();
            try {
                options.setDeviceName(ConfigReader.getAndroidDeviceName());
                options.setApp(ConfigReader.getAndroidApk());
                appiumDriverFactory.androidDriver = new AndroidDriver(new URL(ConfigReader.getRemoteURL()), options);
                appiumDriverFactory.androidDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            }catch (Exception e)
            {
                e.getStackTrace();
            }

        }else if (mobileType.equals(MobileType.IOS))
        {
            XCUITestOptions options = new XCUITestOptions();
            try {
                options.setPlatformName(ConfigReader.getPlatformName());
                options.setAutomationName(ConfigReader.getAutomationName());
                options.setDeviceName(ConfigReader.getIOSDeviceName());
                options.setUdid(ConfigReader.getUdId());
                options.setBundleId(ConfigReader.getBundleId());
                options.setWdaLaunchTimeout(Duration.ofSeconds(20));
                appiumDriverFactory.iosDriver = new IOSDriver(new URL(ConfigReader.getRemoteURL()), options);
                appiumDriverFactory.iosDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

            }catch (Exception e)
            {
                e.getStackTrace();
            }
        }
    }
}
