package appRunner;
import behavior.appBehavior;
import org.testng.annotations.Test;
import steps.android.*;
import utils.android.AndroidBase;
import utils.android.RequestBackendServiceAndroid;
import utils.CompareResults;

public class HCircleAppRunner_Android extends AndroidBase{

    @Test(priority = 1)
    public void LoginAndLogout()
    {
        LoginStepAndroid.loginStep();
        LogoutStepAndroid.logoutStep();
    }
    @Test(priority = 2)
    public void OnLineQRCode()
    {
        LoginStepAndroid.loginStep();
        qrCodeStepAndroid.qrCodeSteps();
        qrCodeStepAndroid.readQRCode();
        RequestBackendServiceAndroid.POST_Simulator_qrCode_First();
        qrCodeStepAndroid.checkSuccessOpen();
        appBehavior.stayBackground();
        qrCodeStepAndroid.readQRCode2();
        RequestBackendServiceAndroid.POST_Simulator_qrCode_Second();
        qrCodeStepAndroid.checkSuccessOpen();
        CompareResults.compareResultsFromAndroid();
    }
    @Test(priority = 3)
    public void OFFLineQRCode()
    {
        LoginStepAndroid.loginStep();
        qrCodeStepAndroid.qrCodeSteps();
        appBehavior.openAirplaneMode();
        appBehavior.tapByCoordinates(500, 2100);
        try {
            Thread.sleep(4000);
        } catch (Exception e) {
            e.getStackTrace();
        }
        appBehavior.stayBackground();
        qrCodeStepAndroid.verifyOfflineQRCode();
        qrCodeStepAndroid.readOfflineQRCode();
        RequestBackendServiceAndroid.POST_Simulator_qrCode_Offline();
        appBehavior.closeAirplaneMode();
        appBehavior.tapByCoordinates(500, 2100);
    }
    @Test(priority = 4)
    public void UpdateProfile()
    {
        LoginStepAndroid.loginStep();
        ProfileStepAndroid.profileStep();
    }
    @Test(priority = 5)
    public void Settings()
    {
        LoginStepAndroid.loginStep();
        SettingsStepAndroid.changedLanguages();
        SettingsStepAndroid.checkTnC();
        SettingsStepAndroid.EnableAutoQRDisplayAtAppStart();
        appBehavior.stayBackground();
        SettingsStepAndroid.EnableAutoQRDisplayAtAppStart();
        appBehavior.stayBackground();
        SettingsStepAndroid.checkUsernameInCodepage();
    }
    @Test(priority = 6)
    public void Register()
    {
        RegisterStepAndroid.LoginStepByRegister();
        RegisterStepAndroid.UserNewSignUp();
    }
    @Test(priority = 7)
    public void LatestHomePage()
    {
        LoginStepAndroid.loginStep();
        HighlightsStepAndroid.highlightsStep();
    }
}
