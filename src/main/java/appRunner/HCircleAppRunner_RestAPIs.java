package appRunner;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.Test;

import java.io.*;
import static java.util.Base64.Encoder;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Scanner;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import static io.restassured.RestAssured.given;

public class HCircleAppRunner_RestAPIs {
    //private static final String devAuthBaseURI = "https://2f02d17a-f2e2-4a9d-8c47-473c5991e83c.hcircle.com.hk/auth";
    //private static final String devBaseURI = "https://2f02d17a-f2e2-4a9d-8c47-473c5991e83c.hcircle.com.hk";
    private static final String devAuthBaseURI = "https://api.dev.exaleap.ai/auth";
    private static final String devBaseURI = "https://api.dev.exaleap.ai";
    private static final String devBaseURIACX = "https://api.dev.exaleap.ai/acx-simulator-svc";
    private static final String acxSimulator = "src/main/resources/api/acxSimulator.json";
    private static final String path = "src/main/resources/api/token.json";
    private static final String timeStampPath = "src/main/resources/api/timeStamp.json";
    private static final String refreshToken11Bytes = "src/main/resources/api/refreshToken11Bytes.txt";
    private static final String secretKeyPath = "src/main/resources/api/secretKey.txt";
    private static final String scrambledCodePath = "src/main/resources/api/scrambledCode.txt";
    private static final String qrCodePath = "src/main/resources/api/qrcode.txt";
    private static final String useTicketPath = "src/main/resources/useTicketToken.json";
    private static final String assetToken = "src/main/resources/assetToken.json";

    public static void POST_Authorize() {
        JSONObject request = new JSONObject();
        request.put("area_code", "852");
        request.put("phone_number", "33336868");
        request.put("organization_identifier", "hld");
        given().
                header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devAuthBaseURI + "/account/authorize").
                then().
                statusCode(200).log().all();
    }

    @Test(priority = 1)
    public void POST_VerifyCode() {
        POST_Authorize();
        JSONObject request = new JSONObject();
        request.put("area_code", "852");
        request.put("phone_number", "33336868");
        request.put("verify_code", "111111");

        Response response = given().
                header("Content-Type", "application/json").contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devAuthBaseURI + "/account/verify-code");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(path))) {
            printWriter.write(response.getBody().prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 2)
    public void GET_AccessScrambleQRCode() {

        JSONParser jsonParser = new JSONParser();
        String accessToken = null;
        String refreshToken = null;
        try {
            FileReader fileReader = new FileReader(path);
            Object object = jsonParser.parse(fileReader);
            JSONObject verifyCodeAPI = (JSONObject) object;
            JSONArray array = (JSONArray) verifyCodeAPI.get("result");
            for (int i = 0; i < array.size(); i++) {
                JSONObject result = (JSONObject) array.get(i);
                accessToken = (String) result.get("access_token");
                refreshToken = (String) result.get("refresh_token");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Response response = given().
                header("Authorization", "Bearer " + accessToken).contentType(ContentType.JSON).accept(ContentType.JSON).
                when().
                get(devBaseURI + "/building-service/access-control-service/access-scramble-qrcode");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(timeStampPath))) {
            printWriter.write(response.getBody().prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }

        String first11Bytes = refreshToken.substring(0, 11);
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(refreshToken11Bytes))) {
            printWriter.write(first11Bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 3)
    public void getSecretKey() {
        File file = new File(refreshToken11Bytes);
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(secretKeyPath))) {
            Scanner scanner = new Scanner(file);
            String refreshToken = scanner.nextLine();
            JSONParser jsonParser = new JSONParser();
            FileReader fileReader = new FileReader(timeStampPath);
            Object object = jsonParser.parse(fileReader);
            JSONObject getCreateTimestamp = (JSONObject) object;
            JSONArray array = (JSONArray) getCreateTimestamp.get("result");
            for (int i = 0; i < array.size(); i++) {
                JSONObject result = (JSONObject) array.get(i);
                long scrambleTs = (long) result.get("create_ts");
                String secretKey = refreshToken + scrambleTs;
                printWriter.write(secretKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 4)
    public void scrambledCode() {
        File file = new File(secretKeyPath);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String secretKey = scanner.nextLine();
        System.out.println(secretKey);
        byte[] key = secretKey.getBytes();

        JSONParser jsonParser = new JSONParser();
        String code = null;
        try {
            FileReader fileReader = new FileReader(timeStampPath);
            Object object = jsonParser.parse(fileReader);
            JSONObject getCreateTimestamp = (JSONObject) object;
            JSONArray array = (JSONArray) getCreateTimestamp.get("result");
            for (int i = 0; i < array.size(); i++) {
                JSONObject result = (JSONObject) array.get(i);
                code = (String) result.get("code");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] data = code.getBytes();
        Key deskey = null;

        DESedeKeySpec spec;
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(scrambledCodePath))) {

            spec = new DESedeKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("desede");
            deskey = keyFactory.generateSecret(spec);

            Cipher cipher = Cipher.getInstance("desede" + "/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, deskey);

            byte[] CipherText = cipher.doFinal(data);
            Encoder encoder = Base64.getEncoder();
            String scrambledCode = encoder.encodeToString(CipherText);
            printWriter.write(scrambledCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 5)
    public void qrCode() {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(qrCodePath))) {
            File file = new File(scrambledCodePath);
            Scanner scanner = null;
            scanner = new Scanner(file);
            JSONParser jsonParser = new JSONParser();
            try {
                FileReader fileReader = new FileReader(timeStampPath);
                Object object = jsonParser.parse(fileReader);
                JSONObject getCreateTimestamp = (JSONObject) object;
                JSONArray array = (JSONArray) getCreateTimestamp.get("result");
                for (int i = 0; i < array.size(); i++) {
                    JSONObject result = (JSONObject) array.get(i);
                    String id = (String) result.get("id");
                    long startTime = (long) result.get("start_time");
                    long endTime = (long) result.get("end_time");
                    long scrambleTs = (long) result.get("create_ts");
                    String scrambledCode = scanner.nextLine();
                    printWriter.write(id + "," + startTime + "," + endTime + "," + scrambleTs + "," + scrambledCode);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (RuntimeException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test(priority = 6)
    public void generateQRCodeImage() {
        File file = new File(qrCodePath);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String data = scanner.nextLine();
        String path = "src/main/resources/api/genQRCode.jpg";

        try {
            BitMatrix matrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, 500, 500);
            MatrixToImageWriter.writeToPath(matrix, "jpg", Paths.get(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 7)
    public void selectTicketByClubhouseDoor() {
        File file = new File(qrCodePath);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String qrcodeString = scanner.nextLine();

        JSONParser jsonParser = new JSONParser();
        String accessToken = null;
        try {
            FileReader fileReader = new FileReader(path);
            Object object = jsonParser.parse(fileReader);
            JSONObject verifyCodeAPI = (JSONObject) object;
            JSONArray array = (JSONArray) verifyCodeAPI.get("result");
            for (int i = 0; i < array.size(); i++) {
                JSONObject result = (JSONObject) array.get(i);
                accessToken = (String) result.get("access_token");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Clubhouse door (公共大門)
//        String device_id = "c322e01b-f0f0-4664-a6b8-57a602165877";
//        String device_data_interface = "access-control.qrcode_validation.clubhouse.door";

//        String device_id = "cbf942af-be42-48b4-96e9-186d5c1e9b32";
//        String device_data_interface = "access-control.qrcode_validation.clubhouse.door";

//         Toilet (洗手間)
//        String device_id = "439185ef-8755-4b6d-a601-cf0b17b28464";
//        String device_data_interface = "access_control.qrcode_validation.toilet.generic";

//        String device_id = "89fd6a17-1c31-4952-b1fe-6acfbb9211d2";
//        String device_data_interface = "access_control.qrcode_validation.toilet.generic";

//         Lift (電梯)
        String device_id = "schindler.generic.lift";
        String device_data_interface = "access_control.qrcode_validation.lift.generic";

//        String device_id = "b389dc5a-09bf-4c4a-b106-bffbf34a0a4b";
//        String device_data_interface = "access_control.qrcode_validation.lift.generic";


        JSONObject request = new JSONObject();
        request.put("device_id", device_id);
        request.put("device_data_interface", device_data_interface);
        request.put("code", qrcodeString);

        Response response = given().
                header("Authorization", "Bearer " + accessToken).contentType(ContentType.JSON).accept(ContentType.JSON).
                body(request.toJSONString()).
                when().
                post(devBaseURIACX + "/qrcode");

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(acxSimulator))) {
            printWriter.write(response.getBody().prettyPrint());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
