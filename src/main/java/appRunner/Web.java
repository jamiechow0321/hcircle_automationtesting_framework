package appRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Web {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","/Users/jamiechow/Downloads/chromedriver_mac64/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("excludeSwitches",new String[]{"enable-automation"});
        options.addArguments("--disable-blink-features=AutomationControlled");
        WebDriver driver = new ChromeDriver(options);
        //driver.navigate().to("https://w2.leisurelink.lcsd.gov.hk/leisurelink/application/checkCode.do?flowId=1&lang=TC");
        driver.navigate().to("https://w2.leisurelink.lcsd.gov.hk/index/index.jsp");
        System.out.println("get link ....");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id='LCSD_1']")).click();
        System.out.println("....done");
    }
}
