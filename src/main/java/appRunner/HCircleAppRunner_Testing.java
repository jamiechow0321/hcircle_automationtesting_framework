package appRunner;

import org.testng.annotations.Test;
import steps.android.LoginStepAndroid;
import steps.android.LogoutStepAndroid;
import utils.android.AndroidBase;

public class HCircleAppRunner_Testing extends AndroidBase{
    @Test(priority = 1)
    public void LoginAndLogout()
    {
        LoginStepAndroid.loginStep();
        LogoutStepAndroid.logoutStep();
    }
}
