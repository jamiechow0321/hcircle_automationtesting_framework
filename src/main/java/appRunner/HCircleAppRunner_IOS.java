package appRunner;
import behavior.appBehavior;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.testng.annotations.Test;
import steps.ios.LoginStepIOS;
import steps.ios.qrCodeStepIOS;
import utils.ios.IOSBase;
import utils.ios.RequestBackendServiceIOS;
import utils.CompareResults;

public class HCircleAppRunner_IOS extends IOSBase{
    ExtentReports extent = new ExtentReports();
    ExtentSparkReporter spark = new ExtentSparkReporter("src/main/resources/rep/Reporting.html");

    @Test
    public void testLogin()
    {
        extent.attachReporter(spark);
        ExtentTest test = extent.createTest("Online QR code flow");
        test.log(Status.INFO, "TEST START...");
        appBehavior.iphoneAlert();
        test.log(Status.PASS, "Step 1.Click alert accept");
        LoginStepIOS.loginStep();
        test.log(Status.PASS, "Step 2.Login success");
    }

    @Test(priority = 1)
    public void testOnlineQRCodeForIOS() {
        extent.attachReporter(spark);
        ExtentTest test = extent.createTest("Online QR code flow");
        test.log(Status.INFO, "TEST START...");
        appBehavior.iphoneAlert();
        test.log(Status.PASS, "Step 1.Click alert accept");
        LoginStepIOS.loginStep();
        test.log(Status.PASS, "Step 2.Login success");
        qrCodeStepIOS.clickGreenBallBtn();
        test.log(Status.PASS, "Step 3.Click entry point by green ball button");
        for (int i = 0; i < 1; i++) {
            qrCodeStepIOS.qrCodeSteps();
            test.log(Status.PASS, "Step 4.Cap screenshot by online QR code picture");
            qrCodeStepIOS.readQRCode();
            test.log(Status.PASS, "Step 5.Get online QR code seed from picture");
            RequestBackendServiceIOS.POST_Simulator_qrCode_First();
            test.log(Status.PASS, "Step 6.Call acx simulator API simulation opening door");
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                e.getStackTrace();
            }
//            appBehavior.stayBackgroundIOS();
//            test.log(Status.PASS, "Step 7.Stay background 5 sec");
//            qrCodeStepIOS.readQRCode2();
//            test.log(Status.PASS, "Step 8.Get online QR code seed from second picture");
//            RequestBackendServiceIOS.POST_Simulator_qrCode_Second();
//            test.log(Status.PASS, "Step 9.Call acx simulator API simulation opening door again");
//            CompareResults.compareResultsFromIOS();
//            test.log(Status.PASS, "Step 10.Compare result is online qr code seed for the first time is different from the online qr code for the second time");
//            appBehavior.stayBackgroundIOS();
//            test.log(Status.PASS, "Step 11.Stay background 5 sec");
//            test.log(Status.INFO, "[Test flow Done...]");
        }
        //appBehavior.removeApp();
//        test.log(Status.INFO, "TEST FINISH");
//        extent.flush();
//
//        appBehavior.scroll();
//        try {
//            Thread.sleep(1000);
//        } catch (Exception e) {
//            e.getStackTrace();
//        }
//        appBehavior.airplaneIOS();
//        try {
//            appBehavior.tapByCoordinatesIOS(500, 2100);
//        }catch (Exception e) {
//            e.getStackTrace();
//        }
//        appBehavior.quitApp();
    }

    @Test(priority = 2)
    public void testOfflineQRCode()
    {
        extent.attachReporter(spark);
        ExtentTest test = extent.createTest("Offline QR code flow");
        test.log(Status.INFO, "TEST START...");
        for (int i = 0; i < 3; i++) {
            qrCodeStepIOS.verifyOfflineQRCode();
            test.log(Status.PASS, "Step 1.Cap screenshot by offline QR code picture");
            qrCodeStepIOS.readOfflineQRCodeIOS();
            test.log(Status.PASS, "Step 2.Get offline QR code seed from picture");
            RequestBackendServiceIOS.POST_Simulator_qrCode_Offline();
            test.log(Status.PASS, "Step 3.Call acx simulator API simulation opening door");
            appBehavior.stayBackgroundIOS();
            test.log(Status.PASS, "Step 4.Stay background 5 sec");
            qrCodeStepIOS.verifyOfflineQRCode2();
            test.log(Status.PASS, "Step 5.Cap screenshot by offline QR code picture again");
            qrCodeStepIOS.readOfflineQRCodeIOS2();
            test.log(Status.PASS, "Step 6.Get offline QR code seed from picture again");
            RequestBackendServiceIOS.POST_Simulator_qrCode_Offline2();
            test.log(Status.PASS, "Step 7.Call acx simulator API simulation opening door again");
            CompareResults.compareResultsFromOfflineQRCode();
            test.log(Status.PASS, "Step 8.Compare result is offline qr code seed for the first time is same from the offline qr code for the second time");
            test.log(Status.INFO, "[Test flow Done...]");}
        test.log(Status.INFO, "TEST FINISH");
        extent.flush();
    }
}
